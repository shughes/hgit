module HGit.Config 
   ( Config
   , initConfig
   , configRemotes

   , Remote
   , initRemote

   , putRemote
   , getRemote
   , putConfig
   , getConfig ) where

import qualified Data.Map as M
import qualified Data.ByteString as B
import qualified Data.ByteString.UTF8 as B2
import qualified HGit.Shared as S
import Data.Typeable
import Data.JSON2
import Data.JSON2.Parser
import Data.Either
import System.Directory

data Config = Config { configRemotes :: M.Map String Remote }
                       deriving Show

data Remote = Remote { remoteUrl :: String
                     , remoteFetch :: String }
                       deriving Show

instance Typeable Remote where
   typeOf remote = mkTyConApp (mkTyCon "HGit.Config.Remote") [] 

instance Typeable Config where
   typeOf config = mkTyConApp (mkTyCon "HGit.Config.Config") []

instance ToJson Config where
   toJson config = 
      let rArr = toJson (configRemotes config)
          jconfig = JObject $ M.fromList [("remotes", rArr)]
      in jconfig

instance ToJson Remote where
   toJson (Remote url fetch) = JObject $ M.fromList 
      [("url", JString url), ("fetch", JString fetch)]

instance FromJson Config where
   safeFromJson json = 
      let Right jmap = safeFromJson json
          Just jremotes = M.lookup "remotes" jmap
          Right remotes = safeFromJson jremotes
      in Right (Config remotes)

instance FromJson Remote where
   safeFromJson (JObject jmap) = 
      let Just (JString url) = M.lookup "url" jmap
          Just (JString fetch) = M.lookup "fetch" jmap
      in Right $ Remote url fetch


putRemote key remote = do
   remotes <- getRemotes
   let newRemotes = M.insert key remote remotes
       newConfig = initConfig newRemotes
   putConfig newConfig

getRemote key = do
   remotes <- getRemotes
   return $ M.lookup key remotes

getRemotes = do 
   config <- getConfig
   return $ configRemotes config


initConfig :: M.Map String Remote -> Config
initConfig remoteMap = Config remoteMap 

initRemote :: String -> String -> Remote
initRemote url fetch = Remote url fetch


putConfig :: Config -> IO ()
putConfig config = do
   gitDir <- S.getGitDir
   let json = toJson config
       str = toString json
       bs = B2.fromString str
       configFile = gitDir ++ "/config"
   B.writeFile configFile bs

getConfig :: IO Config
getConfig = do
   gitDir <- S.getGitDir
   let configFile = gitDir ++ "/config"
   exists <- doesFileExist configFile
   case exists of 
        True -> do
           bs <- B.readFile configFile
           let file = B2.toString bs
           return $ fromJson $ encodeJson file
        False -> return $ initConfig M.empty 
