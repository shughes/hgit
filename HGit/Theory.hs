
-- powerset functor
-- P: Set -> Set

-- P(f): P(A) -> P(B)
-- unit: S -> P(S)
-- join: P(P(S)) -> P(S)
--


class (Functor m) => MyMonad m where
   unit :: a -> m a
   join :: m (m a) -> m a
   bind :: m a -> (a -> m b) -> m b
   bind ma f = join $ fmap f ma

data Set a = Set [a] deriving Show

-- fmap :: (a -> b) -> (Set a) -> (Set b)
instance Functor Set where
   fmap f (Set []) = Set []
   fmap f (Set (a:rest)) = let r = f a
                               Set result = fmap f (Set rest)
                           in Set (r:result)


instance MyMonad Set where
   unit a = Set [a]

   join (Set []) = Set []
   join (Set (Set arr' : rest)) = let (Set arr) = join $ Set rest
                                  in Set (arr' ++ arr)


data State s a = State { runS :: (s -> (a, s)) }

instance MyMonad (State s) where
   unit a = State (\ st -> (a, st))

   join (State sf) = State $ \st ->
      let (sf2, st2) = sf st
      in (runS sf2) st2

   -- bind ma f = join $ fmap f ma

-- fmap :: (a -> b) -> (State s a -> State s b)
-- (X -> Y) -> (F(X) -> F(Y))
instance Functor (State s) where
   fmap f (State sf) = State $ \st ->
      let (a, st2) = sf st
      in (f a, st2)

{- Monad Definition:
 - Functor for Category M where F : M -> M
 - Include the following morphisms:
 -   unit : X -> M(X)
 -   join : M(M(X)) -> M(X)
 -
 - First Law: join . fmap join = join . join
 -
 -}

-- X = [[25],[35]]
--
-- join . fmap join [[25],[35]] 
-- join [25,35]
--
-- join . join [[25],[35]]
-- join [25,35]
--

-- join M(M(X)) -> M(X)
-- F : A -> B
-- F :      A       ->          B
-- X -> F(X)
-- F : (f : x -> y) -> (F(f) : F(x) -> F(y))

(->>) a f = bind a f

get = State $ \st -> (st, st)
put x = State $ \st -> ((), x)

test = 
   get ->> \arr ->
   put (25:arr) ->> \_ ->
   get ->> \arr ->
   put (35:arr) ->> \_ ->
   get
