module HGit.Stage where

import HGit.Blob
import HGit.Tree
import HGit.Index
import HGit.Commit
import qualified HGit.Shared as S
import qualified Data.ByteString as B
import qualified Data.ByteString.UTF8 as B2
import qualified Data.Map as M
import qualified Data.List as L
import Data.Serialize
import Directory

data Object = TreeType Tree
            | BlobType Blob
            | CommitType Commit
            | EmptyType
              deriving Show


getObject :: S.ObjectHash -> IO Object
getObject path = do
   mblob <- getBlob path
   maybe
      (do mtree <- getTree path
          maybe (return EmptyType) (return . TreeType) mtree)
      (return . BlobType)
      mblob

putFile :: Index -> IO ()
putFile index = do
   bs <- getContent index
   rootDir <- S.rootDir
   B.writeFile (rootDir ++ (indexFile index)) bs

indexToFiles :: IO ()
indexToFiles = do
   arr <- getIndexed
   indexArrToFiles arr

indexArrToFiles :: IndexArr -> IO ()
indexArrToFiles arr = do
   mapM_ (\ index -> do
            root <- S.rootDir
            let file = indexFile index
                path = S.removeFileName file
                arr = S.splitPath root path
            recur root arr
            putFile index)
         (indexArr arr)
   where
      recur fullPath [] = return ()
      recur fullPath (path : arr) = do
         let p = fullPath ++ "/" ++ path
         exists <- doesDirectoryExist p
         case exists of
              True -> recur p arr
              False -> do createDirectory p
                          recur p arr


indexedFileToWorkingTree file = do
   indexed <- getIndexed
   let indexMap = M.fromList $ map 
         (\ index -> (indexFile index, index)) (indexArr indexed)
       Just index = M.lookup file indexMap
   putFile index

objectsToIndex :: IO ()
objectsToIndex = do
   arr' <- getHeadCommitTree
   putIndexArr arr'

getCommitTreeHash :: IO (Maybe S.ObjectHash)
getCommitTreeHash = do
   mhead <- S.getHeadHash
   maybe
      (return Nothing)
      (\ head -> do
         commit <- getCommit head
         return $ Just (commitTree commit))
      mhead

checkoutBranch :: String -> IO ()
checkoutBranch name = do
   S.setCurrentBranch name
   Just commit <- S.getHeadHash
   checkoutInCurBranch commit

checkoutInCurBranch :: S.ObjectHash -> IO ()
checkoutInCurBranch hash = do
   S.setHeadHash hash
   objectsToIndex
   indexToFiles
   

checkout :: String -> IO ()
checkout hash = do
   branches <- S.getBranches
   case (L.elem hash branches) of
        True -> checkoutBranch hash
        False -> checkoutNoBranch hash

checkoutNoBranch :: S.ObjectHash -> IO ()
checkoutNoBranch hash = do
   S.rmHeadRef
   checkoutInCurBranch hash


getObjectByCommit :: S.ObjectHash -> FilePath -> IO (S.ObjectHash, Object)
getObjectByCommit commitHash filePath = do
   c <- getCommit commitHash
   let treeHash = commitTree c
   arr <- getCommitTree True "" treeHash
   let fileHash = foldl
         (\ h i -> S.if_ ((indexFile i)==filePath) (Just $ indexHash i) h)
         Nothing (indexArr arr)
   maybe
      (return ("", EmptyType))
      (\ fhash -> do obj <- getObject fhash
                     return (fhash, obj))
      fileHash


checkoutFile :: S.ObjectHash -> FilePath -> IO ()
checkoutFile commitHash filePath = do
   (hash, obj) <- getObjectByCommit commitHash filePath
   handleObject hash obj
   where
      handleObject _ (BlobType blob) = do
         rootDir <- S.rootDir
         B.writeFile (rootDir ++ filePath) (blobContent blob)
      handleObject fhash (TreeType tree) = do
         arr <- getCommitTree False filePath fhash
         indexArrToFiles arr

resetFileMixed commitHash file = do
   (hash, _) <- getObjectByCommit commitHash file
   indexed <- getIndexed
   let newIndexed = map 
         (\ index -> 
            S.if_ ((indexFile index)==file) (newIndex hash file Nothing) index) 
         (indexArr indexed)
   putIndexArr (newIndexArr newIndexed)


resetSoft :: S.ObjectHash -> IO ()
resetSoft hash = S.setHeadHash hash

resetMixed :: S.ObjectHash -> IO ()
resetMixed hash = do
   S.setHeadHash hash
   objectsToIndex

resetHard :: S.ObjectHash -> IO ()
resetHard hash = do
   checkoutInCurBranch hash
   indexed <- getIndexed
   wtree <- getWorkingTree
   let toRemove = getUnindexed indexed wtree
   rootDir <- S.rootDir
   mapM_ (removeFile . ((rootDir ++) . indexFile))
         (indexArr toRemove)


getStatus :: IO (IndexArr, IndexArr, IndexArr, IndexArr)
getStatus = do
   wtree <- getWorkingTree
   indexed <- getIndexed
   arr1 <- getChangesToCommit indexed
   let arr2 = getUnindexed indexed wtree
       arr3 = getUntracked indexed wtree
       arr4 = getRemoved indexed wtree
   return (arr1, arr2, arr3, arr4)

getUntracked :: IndexArr -> IndexArr -> IndexArr
getUntracked indexed' wtree =
   let unindexed' = getUnindexed indexed' wtree
       unindexed = indexArr unindexed'
       indexed = indexArr indexed'
       indexMap = M.fromList $ map (\ i -> (indexFile i, True)) indexed
       untracked = foldl
            (\ arr i ->
               let mexists = M.lookup (indexFile i) indexMap
               in maybe (i : arr) (\ _ -> arr) mexists)
            [] unindexed
   in newIndexArr untracked

getHeadCommitTree :: IO IndexArr
getHeadCommitTree = do
   mhash <- getCommitTreeHash
   maybe
      (return $ newIndexArr [])
      (getCommitTree False "")
      mhash

getChangesToCommit :: IndexArr -> IO IndexArr
getChangesToCommit arr2 = do
   arr1 <- getHeadCommitTree
   let map1 = M.fromList $ map (\i -> (indexHash i, True)) $ indexArr arr1
   return $ newIndexArr $ foldl
      (\ arr index ->
         let mval = M.lookup (indexHash index) map1
             newArr = maybe (index : arr) (\_ -> arr) mval
         in newArr)
      [] (indexArr arr2)


getCommitTree :: Bool -> FilePath -> String -> IO IndexArr
getCommitTree includeTrees path hash = do
   Just tree <- getTree hash
   let rows = flatRows tree
   arr <- recurTree path rows
   return $ newIndexArr arr
   where
      recurTree fullPath [] = return []
      recurTree fullPath ((hash, path, ttype) : siblings) = do
         let newPath = fullPath ++ "/" ++ path
         case ttype of
              "tree" -> do result' <- getCommitTree includeTrees newPath hash
                           let result = indexArr result'
                           treeResult <- recurTree fullPath siblings
                           let result2 = case includeTrees of
                                              True -> [newIndex hash newPath Nothing]
                                                       ++ result ++ treeResult
                                              False -> result ++ treeResult
                           return result2
              "blob" -> do result <- recurTree fullPath siblings
                           Just blob <- getBlob hash
                           let content = blobContent blob
                               node = newIndex hash newPath Nothing
                           return $ [node] ++ result

