import HGit.Command 

import qualified Data.Map as M
import qualified Data.ByteString as B

import System.Environment

main = do 
   args <- getArgs
   parseCommand args
   return ()
