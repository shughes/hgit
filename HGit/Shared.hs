module HGit.Shared where

import qualified Data.ByteString as B
import qualified Data.ByteString.UTF8 as B2
import qualified Data.ByteString.Lazy as BL
import qualified Data.List as L
import System.Directory
import Data.Word
import Data.Ratio
import Data.Digest.Pure.SHA

type ObjectHash = String

isGitDir ('/':'.':'h':'g':'i':'t':rest) = True
isGitDir other = False

gitDir = "hgit/.hgit"

getGitDir = do
   cur <- getCurrentDirectory
   case cur of
        "" -> return ""
        otherwise -> iterPath cur
   where
      iterPath cur = do
         exists <- doesDirectoryExist $ cur ++ "/" ++ gitDir
         case exists of
              True -> return $ cur ++ "/" ++ gitDir
              False -> iterPath (removeFileName cur)


configFile = do
   gdir <- getGitDir
   return $ gdir ++ "/config"

indexPath = do
   gdir <- getGitDir
   return $ gdir ++ "/index"

objectsDir = do
   gdir <- getGitDir
   return $ gdir ++ "/objects"

rootDir :: IO FilePath
rootDir = do
   gdir <- getGitDir
   let root = removeFileName gdir
   return root

headRef = "HEAD"

currentHead = do
   gdir <- getGitDir
   return $ gdir ++ "/" ++ headRef

mergeHead = do
   gdir <- getGitDir
   return $ gdir ++ "/MERGE_HEAD"

stashPath = "refs/stash"

headsDir = "refs/heads"

author = "Sam Hughes"

getBranches :: IO [FilePath]
getBranches = do
   gdir <- getGitDir
   dirs <- getDirectoryContents (gdir ++ "/" ++ headsDir)
   let branches = foldl (\ arr dir -> if_ (or [dir==".", dir==".."])
                           arr (arr ++ [dir])) [] dirs
   return $ map ((headsDir++"/")++) branches


rmBranch :: FilePath -> IO ()
rmBranch name = do
   gdir <- getGitDir
   removeFile (gdir ++ "/" ++ name)

rmHeadRef = do
   cur <- currentHead
   removeFile cur

setCurrentBranch :: String -> IO ()
setCurrentBranch name = do
   cur <- currentHead
   setRef cur name

getCurrentBranch :: IO String
getCurrentBranch = do
   cur <- currentHead
   mpath <- getRef cur
   maybe
      (do Just hash <- getHeadHash
          return hash)
      return
      mpath


getFile :: FilePath -> IO (Maybe String)
getFile path = do
   exists <- doesFileExist path
   if_ exists
      (do bs <- B.readFile path
          return (Just $ B2.toString bs))
      (return Nothing)

getHash :: FilePath -> IO (Maybe ObjectHash)
getHash path = do
   mref <- getRef path
   gdir <- getGitDir
   hash <- maybe
      (getFile path)
      (getFile . ((concat [gdir, "/"]) ++))
      mref
   return $ maybe Nothing (Just . trim) hash

setRef :: FilePath -> FilePath -> IO ()
setRef path refPath = do
   let bs = B2.fromString ("ref: " ++ refPath)
   B.writeFile path bs

getRef :: FilePath -> IO (Maybe FilePath)
getRef path = do
   mstr <- getFile path
   return $ maybe Nothing parseStr mstr
   where
      parseStr ('r':'e':'f':':': rest) = Just $ trim rest
      parseStr other = Nothing

createBranch :: String -> IO ()
createBranch name = do
   mhash <- getHeadHash
   gdir <- getGitDir
   B.writeFile (gdir ++ "/" ++ name) $ maybe (B2.fromString "") B2.fromString mhash

getStashHash :: IO (Maybe ObjectHash)
getStashHash = do
   gdir <- getGitDir
   getHash (gdir ++ "/" ++ stashPath)

setHeadHash :: ObjectHash -> IO ()
setHeadHash hash = do
   cur <- currentHead
   mpath <- getRef cur
   let bs = B2.fromString hash
   gdir <- getGitDir
   maybe
      (B.writeFile cur bs)
      (\ path -> B.writeFile (gdir ++ "/" ++ (trim path)) bs)
      mpath

getHeadHash :: IO (Maybe ObjectHash)
getHeadHash = do
   cur <- currentHead
   getHash cur

rmMergeBranch :: IO ()
rmMergeBranch = do
   mhead <- mergeHead
   removeFile mhead

setMergeBranch :: String -> IO ()
setMergeBranch branch = do
   gdir <- getGitDir
   mfile <- getFile (gdir ++ "/" ++ branch)
   mhead <- mergeHead
   maybe
      (B.writeFile mhead $ B2.fromString branch)
      (\_ -> setRef mhead branch)
      mfile

getMergeBranch :: IO (Maybe String)
getMergeBranch = do
   mhead <- mergeHead
   getHash mhead

hashString :: String -> ObjectHash
hashString str =
   let bs = B2.fromString str
       sum = hashBs bs
   in sum

hashBs :: B.ByteString -> ObjectHash
hashBs bs = 
   let w = B.unpack bs
       bs2 = BL.pack w
       sha = sha256 bs2 
   in L.take 32 $ showDigest sha

splitPath :: FilePath -> FilePath -> [FilePath]
splitPath root [] = []
splitPath root path = result
    where
      dirTuple = break (\x -> x=='/') path
      newPath = case length $ snd dirTuple of
                  0 -> ""
                  otherwise -> tail $ snd dirTuple
      result = case (fst dirTuple) == root of
                 False -> [(fst dirTuple)] ++ (splitPath root newPath)
                 True -> splitPath root newPath

split c [] = []
split c str =
   let (a, b) = break (==c) str
       newStr = if_ ((length b) == 0) "" (tail b)
   in (a : split c newStr)

if_ :: Bool -> a -> a -> a
if_ True  x _ = x
if_ False _ y = y

trim str =
   let s1 = dropWhile rm str
       s2 = dropWhile rm (L.reverse s1)
   in L.reverse s2
   where
      rm '\n' = True
      rm ' ' = True
      rm other = False


removeFileName :: FilePath -> FilePath
removeFileName filePath =
   let revPath = reverse filePath
       (_, count) = foldl (\ (continue, count) c ->
           case continue of
                True -> case c of
                             '/' -> (False, count+1)
                             otherwise -> (continue, count+1)
                False -> (False, count))
           (True, 0) revPath
       pathLength = length filePath
       filePath2 = take (pathLength - count) filePath
   in filePath2
