import Data.Traversable
import Data.Foldable
import Control.Applicative
import Data.Monoid

data Tree a = Empty | Leaf a | Node (Tree a) a (Tree a) deriving Show

instance Functor Tree where
   fmap f Empty = Empty
   fmap f (Leaf x) = Leaf (f x)
   fmap f (Node l k r) = Node (fmap f l) (f k) (fmap f r)

instance Foldable Tree where
   foldMap f Empty = mempty
   foldMap f (Leaf x) = f x
   foldMap f (Node l k r) = foldMap f l `mappend` f k `mappend` foldMap f r

instance Traversable Tree where
   traverse f Empty = pure Empty
   traverse f (Leaf x) = Leaf <$> f x
   
   -- sam test
   -- hello
