module HGit.Config 
   ( getAuthor ) where


import qualified Data.Map as M
import qualified Data.ByteString as B
import qualified Data.ByteString.UTF8 as B2
--import qualified HGit.Shared as S
import Data.Typeable
--import Data.JSON2
--import Data.JSON2.Parser
import Data.Either
import qualified Data.List as L
import System.Directory
import Text.ParserCombinators.Parsec
import Text.ParserCombinators.Parsec.Char


getAuthor :: IO String
getAuthor = return "Sam Hughes"

-- GenParser tok st a
-- Parser a = GenParser Char () a
-- CharParser st a = GenParser Char st a
test :: CharParser st Char
test = do
   char '{'
   anyChar
   anyChar
   anyChar
   char '}'


data Config = Config { configRemotes :: String
                     , configAuthor :: String }
                       deriving Show

--encode :: (JSerialize a) => a -> String
--encode val = execJState (put val) []


{-
   data Remote = Remote { remoteUrl :: String
                     , remoteFetch :: String }
                       deriving Show


instance Typeable Remote where
   typeOf remote = mkTyConApp (mkTyCon "HGit.Config.Remote") [] 

instance Typeable Config where
   typeOf config = mkTyConApp (mkTyCon "HGit.Config.Config") []

instance ToJson Config where
   toJson config = 
      let rArr = toJson (configRemotes config)
          jconfig = JObject $ M.fromList [("remotes", rArr), ("author", toJson (configAuthor config))]
      in jconfig

instance ToJson Remote where
   toJson (Remote url fetch) = JObject $ M.fromList 
      [("url", JString url), ("fetch", JString fetch)]

instance FromJson Config where
   safeFromJson json = 
      let Right jmap = safeFromJson json
          Just jremotes = M.lookup "remotes" jmap
          Just jauthor = M.lookup "author" jmap
          Right remotes = safeFromJson jremotes
          author = fromJson jauthor
      in Right (Config remotes author)

instance FromJson Remote where
   safeFromJson (JObject jmap) = 
      let Just (JString url) = M.lookup "url" jmap
          Just (JString fetch) = M.lookup "fetch" jmap
      in Right $ Remote url fetch


putRemote key remote = do
   config <- getConfig
   let remotes = configRemotes config
       newRemotes = M.insert key remote remotes
       author = configAuthor config
       newConfig = initConfig newRemotes author
   putConfig newConfig

getRemote key = do
   remotes <- getRemotes
   return $ M.lookup key remotes

getRemotes = do 
   config <- getConfig
   return $ configRemotes config

getAuthor = do
   config <- getConfig
   return $ configAuthor config

putAuthor author = do
   config <- getConfig
   let newConfig = (initConfig (configRemotes config) author)
   putConfig newConfig

initConfig :: M.Map String Remote -> String -> Config
initConfig remoteMap author = Config remoteMap author

initRemote :: String -> String -> Remote
initRemote url fetch = Remote url fetch


putConfig :: Config -> IO ()
putConfig config = do
   gitDir <- S.getGitDir
   let json = toJson config
       str = toString json
       bs = B2.fromString str
       configFile = gitDir ++ "/config"
   B.writeFile configFile bs

getConfig :: IO Config
getConfig = do
   gitDir <- S.getGitDir
   let configFile = gitDir ++ "/config"
   exists <- doesFileExist configFile
   case exists of 
        True -> do
           bs <- B.readFile configFile
           let file = B2.toString bs
           return $ fromJson $ encodeJson file
        False -> return $ initConfig M.empty ""
-}
