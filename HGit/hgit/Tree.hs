module HGit.Tree 
   ( Tree
   , flatRows
   , createTree
   , getTree
   , getTrees ) where

import qualified HGit.Shared as S
import HGit.Index
import qualified Data.Map as M
import qualified Data.ByteString as B
import Data.Serialize
import Directory



data Tree = BlobRow { treeFileHash :: String 
                    , treeFilePath :: FilePath }
          | FlatTreeRow [(String, String, String)]
          | Tree { treeFilePath :: FilePath
                 , treeSubTrees :: [Tree] }
          | EmptyRow
            deriving (Show, Eq)

instance Serialize Tree where
   put (Tree path sub) = do
      putWord8 1
      put path
      put sub

   put (BlobRow hash path) = do
      putWord8 2
      put hash
      put path

   put (FlatTreeRow tuple) = do
      putWord8 3
      put tuple

   get = do
      treeType <- getWord8
      case treeType of
           1 -> do path <- get
                   sub <- get
                   return $ Tree path sub
           2 -> do hash <- get
                   path <- get
                   return $ BlobRow hash path
           3 -> do tuple <- get
                   return $ FlatTreeRow tuple
           otherwise -> return EmptyRow



flatRows (FlatTreeRow rows) = rows

getTree :: S.ObjectHash -> IO (Maybe Tree)
getTree hash = do
   objectsDir <- S.objectsDir
   bs <- B.readFile (objectsDir ++ "/" ++ hash)
   let tree = decode bs
       result = either 
         (\_ -> Nothing) 
         (\t -> Just t)
         tree
   return result

getTrees = do
   arr <- createTree
   let jmap = mapTree arr M.empty
       root = S.hashBs $ encode $ head arr
   return (root, jmap)

toFlatRow (Tree _ trees) = FlatTreeRow $ map getRow trees
   where
      getRow tree@(Tree p s) = let h = S.hashBs $ encode tree
                               in (h, p, "tree")
      getRow (BlobRow h p) = (h, p, "blob")

mapTree [] jmap = jmap
mapTree (BlobRow _ _ : siblings) jmap = mapTree siblings jmap
mapTree (tree@(Tree path subTrees) : siblings) jmap = 
   let json = toFlatRow tree
       hash = S.hashBs $ encode tree
       jmap' = M.insert hash json jmap
       jmap2 = mapTree subTrees jmap'
   in mapTree siblings jmap2
       
newRootTree = Tree "" []

createTree = do
   indexes <- getIndexed
   rootDir <- S.rootDir
   let rootTree = indexToTree rootDir (indexArr indexes) newRootTree
   return $ treeSubTrees rootTree

indexToTree root [] tree = tree
indexToTree root (index:indexes) (Tree treePath subTrees) = 
   let file = indexFile index
       hash = indexHash index
       pathArr = S.splitPath root file
       subTrees' = recurTree hash pathArr subTrees
   in indexToTree root indexes (Tree treePath subTrees')


recurTree newHash (blob:[]) siblings = siblings ++ [(BlobRow newHash blob)]
recurTree newHash (curPath:pathArr) [] = [(Tree curPath (recurTree newHash pathArr []))]
recurTree newHash (curPath:pathArr) (Tree path subTrees : siblings) = 
   case curPath == path of
        True -> ((Tree path (recurTree newHash pathArr subTrees)) : siblings)
        False -> (Tree path subTrees : recurTree newHash (curPath:pathArr) siblings)
recurTree newHash (curPath:pathArr) (other : siblings) = 
   (other : recurTree newHash (curPath:pathArr) siblings)
   




