module HGit.Merge where

import HGit.Stage
import HGit.Index
import HGit.Blob
import HGit.Commit
import HGit.Tree
import qualified HGit.Shared as S
import qualified Data.ByteString as B
import qualified Data.ByteString.UTF8 as B2
import qualified Data.Map as M
import qualified Data.List as L
import Data.Algorithm.Diff 


data MergeType = C String String 
               | G String 
                 deriving Show

patch :: [Index] -> S.ObjectHash -> S.ObjectHash -> S.ObjectHash -> IO [(FilePath, String)]
patch arr commonHash curHash otherHash = 
   mapM (\ i -> do
      (_, BlobType cur) <- getObjectByCommit curHash (indexFile i)
      (_, BlobType other) <- getObjectByCommit otherHash (indexFile i)

      let curStr = B2.toString $ blobContent cur
          otherStr = B2.toString $ blobContent other

          curRows = S.split '\n' curStr
          otherRows = S.split '\n' otherStr

          diffArr = getGroupedDiff curRows otherRows
          fileStr = foldl parseDiff "" diffArr

      return (indexFile i, fileStr))
      arr
   where
      parseDiff str (B, arr) = str ++ (combine arr)
      parseDiff str (F, arr) = str ++ "\n>>>>>>>>>> HEAD\n" ++ 
         (combine arr) ++ "\n<<<<<<<<<<\n"
      parseDiff str (S, arr) = str ++ "\n>>>>>>>>>> MERGE\n" ++
         (combine arr) ++ "\n<<<<<<<<<<\n"

      combine arr = L.concat $ L.intersperse "\n" arr



merge :: String -> IO [FilePath]
merge otherBranch = do
   S.setMergeBranch otherBranch
   Just otherHash <- S.getMergeBranch
   otherCommit <- getCommit otherHash
   Just curHead <- S.getHeadHash 
   Just (commonHash, commonParent) <- getCommonCommit 
      (initCommitParent otherHash) (initCommitParent curHead)
   commonCommit <- getCommit commonHash

   curTree <- getHeadCommitTree 
   otherTree <- getCommitTree False "" (commitTree otherCommit)
   commonTree <- getCommitTree False "" (commitTree commonCommit)

   let commonMap = toMap (indexArr commonTree) 
       curChanged = foldl (whatChanged commonMap) [] (indexArr curTree)
       otherChanged = foldl (whatChanged commonMap) [] (indexArr otherTree)
       curChangedMap = toMap curChanged
       requiresMerge = foldl
          (\ arr i ->
             let commonHash = M.lookup (indexFile i) curChangedMap
                 isEqual = case commonHash of
                                Nothing -> True
                                otherwise -> commonHash == (Just $ indexHash i)
             in S.if_ isEqual arr (i : arr))
          [] otherChanged


   combined <- patch requiresMerge commonHash curHead otherHash

   mapM putFile otherChanged
   mapM (addFile . indexFile) otherChanged

   -- write files with conflict
   conflicts <- mapM 
      (\ (file, content) -> do
         let bs = B2.fromString content
         rootDir <- S.rootDir
         B.writeFile (rootDir ++ file) bs
         return file)
      combined

   return conflicts

   where
      whatChanged commonMap arr i = 
         let commonHash = M.lookup (indexFile i) commonMap
             isEqual = commonHash == (Just $ indexHash i)
         in S.if_ isEqual arr (i : arr)

      toMap arr = M.fromList $ map (\ i -> (indexFile i, indexHash i)) arr

