{-# LANGUAGE PackageImports #-}

import Data.Traversable
import Data.Foldable
import Control.Applicative
--import "mtl" Control.Monad.State
import Control.Monad
import Data.Monoid

{- 
 - var state;
 - function get() { return state; }
 - function put(x) { state = x; }
 - function doState() { 
 -   var x = get();
 -   put(x+1);
 -   return x;
 - }
 -}


main = do
   error "this is an error"

newtype State s a = State { runS :: (s -> (a, s)) }

{-
sf >>= f = State $ \st ->
   let (a, st2) = runS sf st
   in runS (f a) st2
-}

bind sf f = State $ \st -> 
   let (a, st2) = runS (State $ \gst -> (gst, gst)) st
   in runS (f a) st2

instance Functor (State s) where
   fmap f sf = State $ \st ->
      let (a, st2) = runS sf st
          b = f a
      in runS (return b) st2

instance Monad (State s) where
   sf >>= f = stJoin (fmap f sf)
   return a = State $ \s -> (a, s)

stJoin :: State s (State s a) -> State s a
stJoin sf = State $ \st ->
   let (ret, st2) = runS sf st
   in runS ret st2

put a = State $ \ st -> ((), a)

get = State $ \ st -> (st, st)


data MyMaybe a = MyJust a | MyNothing deriving Show

instance Functor MyMaybe where
   fmap f (MyJust x) = let r = f x
                       in MyJust r
   fmap f MyNothing = MyNothing

instance Monad MyMaybe where
   mb >>= f = join_ (fmap f mb)
   return x = MyJust x

join_ :: MyMaybe (MyMaybe a) -> MyMaybe a
join_ (MyJust m) = m
join_ MyNothing = MyNothing


--fmap2 f (Just x) (Just y) = Just (f x y)
fmap3 f (Just x) (Just y) (Just z) = Just (f x y z)
-- f = \ x y -> 2 + x * y
fmap2 f a b = f <$> a <*> b


data IO' a = IO' { runIO :: (IOState -> (IOState, a)) }

instance Monad IO' where
   (IO' sF) >>= f = IO' $ \st -> 
      let (st2, a2) = sF st
          (IO' sF2) = f a2
      in sF2 st2

   return a = IO' $ \s -> (s, a)

data IOState = IOState { ioIn :: [String]
                       , ioOut :: [String]
                       , ioErr :: [String] }
                         deriving Show

print' :: String -> IO' ()
print' str = IO' $ \ (IOState i o e) -> (IOState i (o++[str]) e, ())

initIOState = IOState [] [] []

testIO = do
   print' "samuel"
   print' "hughes"

repl f = 
   let (st, a) = runIO f initIOState
   in ioOut st
