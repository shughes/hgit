module HGit.JSON 
   ( JSON
   , jobject
   , jarray
   , jint
   , jdouble
   , jstring

   , initObject
   , initArray
   , initInt
   , initDouble
   , initString

   , parseJson
   , encode ) where

import qualified Data.Map as M
import qualified Data.List as L
import qualified Data.ByteString as B
import qualified Data.ByteString.UTF8 as B2
import Text.ParserCombinators.Parsec

--
-- type Parser a = GenParser Char () a
--
-- newtype GenParser tok st a = Parser (State tok st -> Consumed (Reply tok st a))
--
-- newtype State st a = State (st -> (a, st))
--
-- data Consumed a = Consumed a | Empty !a
--
-- data Reply tok st a = Ok !a !(State tok st) ParseError | Error ParseError
--
-- data State tok st = State { stateInput :: [tok], statePos :: !SourcePos, stateUser :: !st }
--

data JSON = JObject [(String, JSON)]
          | JArray [JSON]
          | JInt Int
          | JDouble Double
          | JString String
            deriving Show

jobject (JObject jmap) = jmap
jarray (JArray arr) = arr
jint (JInt ji) = ji
jdouble (JDouble jd) = jd
jstring (JString js) = js

initObject jmap = JObject jmap
initArray jarr = JArray jarr
initInt ji = JInt ji
initDouble jd = JDouble jd
initString js = JString js

jsonVal :: GenParser Char st JSON
jsonVal = do
   spaces
   result <- stringVal <|> array <|> object <|> numberVal
   spaces
   return result

array :: GenParser Char st JSON
array = do
   char '['
   arr <- sepBy jsonVal (char ',')
   char ']'
   return $ JArray arr

stringVal = do
   char '"'
   val <- stringIter
   char '"'
   return $ JString val

stringIter :: GenParser Char st String
stringIter = do
   val <- many $ noneOf "\\\""
   (do v <- string "\\\""
       val2 <- stringIter
       return $ val ++ v ++ val2) <|> (return val)

numberVal :: GenParser Char st JSON
numberVal = do
   num <- many digit
   (do char '.'
       num2 <- many digit
       return $ JDouble $ read (num ++ "." ++ num2)) <|> (return $ JInt $ read num)

object :: GenParser Char st JSON
object = do
   char '{'
   result <- sepBy keyVal (char ',')
   char '}'
   return $ JObject result

keyVal :: GenParser Char st (String, JSON)
keyVal = do
   JString key <- jsonVal
   char ':'
   val <- jsonVal
   return (key, val)

parseJson :: String -> Either ParseError JSON
parseJson input = parse jsonVal "(unknown)" input


encode :: JSON -> String
encode (JArray arr) = 
   let result = map (\ val -> encode val) arr
       result2 = foldl (\ arr2 str -> arr2 ++ "," ++ str) "" result
       result3 = "[" ++ (L.tail $ result2 ++ "]")
   in result3

encode (JObject objMap) = 
   let arr = map (\ (key, val) -> "\""++key++"\": " ++ (encode val)) objMap
       result = foldl (\ arr2 str -> arr2 ++ "," ++ str) "" arr
       result2 = "{" ++ (L.tail $ result ++ "}")
   in result2

encode (JString str) = show str
encode (JInt i) = show i
encode (JDouble d) = show d
