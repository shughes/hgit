module HGit.Command where

import HGit.Index
import HGit.Stage
import HGit.Stash
import HGit.Merge
import HGit.Commit
import HGit.Config
import qualified HGit.Shared as S
import qualified Data.Map as M
import qualified Data.List as L
import qualified Data.ByteString as B
import qualified Data.ByteString.UTF8 as B2
import System.IO
import System.Directory


hgit str = do
   copyForTest
   let arr = S.split ' ' str
   parseCommand arr

copyForTest = do
   cur <- getCurrentDirectory
   content <- getDirectoryContents cur
   mapM_ (\ file -> do
      case file of
           "." -> return ()
           ".." -> return ()
           "hgit" -> return ()
           otherwise -> copyFile file ("hgit/"++file))
      content

parseCommand :: [String] -> IO ()

parseCommand ("init":rest) = do 
   exists <- doesDirectoryExist S.gitDir
   case exists of
        True -> return ()
        False -> do createDirectory S.gitDir
                    createDirectory (S.gitDir ++ "/objects")
                    createDirectory (S.gitDir ++ "/refs")
                    createDirectory (S.gitDir ++ "/" ++ S.headsDir)
                    B.writeFile (S.gitDir ++ "/" ++ S.firstCommitFile) (B2.fromString "")


parseCommand ("config":"put":"remote":key:fetch:url:rest) = putRemote key (initRemote fetch url)

parseCommand ("log":hash:rest) = doFunc logIO hash
parseCommand ("log":rest) = doFunc logIO "HEAD"

parseCommand ("status":rest) = statusIO

parseCommand ("reset":"--soft":hash:rest) = doFunc resetSoft hash
parseCommand ("reset":"--hard":hash:rest) = doFunc resetHard hash
parseCommand ("reset":"--mixed":hash:rest) = doFunc resetMixed hash
parseCommand ("reset":hash:rest) = doFunc resetMixed hash


parseCommand ("add":".":rest) = addAllFiles
parseCommand ("add":file:rest) = addFile file

parseCommand ("rm":file:rest) = rmFile file

parseCommand ("commit":message) = 
   commit (S.trim (foldl ((++) . (++ " ")) "" message))

parseCommand ("branch":rest) = branchIO

parseCommand ("checkout":"-b":branch:hash:rest) = do
   let fullBranch = (S.headsDir ++ "/" ++ branch)
   S.createBranch fullBranch
   S.setCurrentBranch fullBranch
   S.setHeadHash hash
   checkout fullBranch
parseCommand ("checkout":branch:rest) = doFunc checkout branch

parseCommand ("merge":branch:rest) = do
   conflicts <- merge branch
   print conflicts
   return ()

parseCommand ("stash":"push":rest) = do
   let message = foldl ((++) . (++" ")) "" rest
   stashPush $ S.trim message
parseCommand ("stash":"pop":rest) = stashPop
parseCommand ("stash":"list":rest) = stashListIO

parseCommand ("help":rest) = hPutStrLn stdout "help"

parseCommand other = parseCommand ["help"]

logIO hash = do
   log <- getLogByHash hash
   hPutStrLn stdout ""
   mapM_ (\ (hash, c) -> do
      hPutStrLn stdout ("commit " ++ hash)
      hPutStrLn stdout (show c))
      log

statusIO = do
   (arr1, arr2, arr3, arr4) <- getStatus
   hPutStrLn stdout "\nChanges to be committed:"
   mapM_ showIndex (indexArr arr1)
   hPutStrLn stdout "\nChanges not staged for commit:"
   mapM_ showIndex (indexArr arr2)
   hPutStrLn stdout "\nUntracked files:"
   mapM_ showIndex (indexArr arr3)
   hPutStrLn stdout ""
   where
      showIndex index = hPutStrLn stdout $ "   " ++ (indexFile index)
         ++ ", " ++ (indexHash index)

stashListIO = do
   lst <- stashList
   mapM_ (hPutStrLn stdout) lst
   return ()

branchIO = do
   cur <- S.getCurrentBranch
   hPutStrLn stdout cur

doFunc f hash =
   case hash of
        "HEAD" -> useHead
        "." -> useHead
        otherwise -> f hash
   where
      useHead = do
         Just head <- S.getHeadHash
         f head
