module HGit.Index
   ( Index
   , indexHash
   , indexFile
   , indexContent
   , newIndex


   , IndexArr
   , indexArr
   , newIndexArr

   , putIndexArr
   , getRemoved
   , getWorkingTree
   , rmFile
   , addFile
   , addAllFiles
   , createIndex
   , getUnindexed
   , getIndexed )
   where


import qualified HGit.Shared as S
import System.Directory
import qualified Data.ByteString as B
import qualified Data.Map as M
import qualified Data.List as L
import Data.Typeable
import Data.Word
import Data.Serialize


data Index = Index { indexHash :: String
                   , indexFile :: FilePath
                   , indexContent :: Maybe B.ByteString }
                   deriving (Eq)

instance Ord Index where
   compare index1 index2  = compare (indexFile index1) (indexFile index2)

instance Serialize Index where
   put index = do
      put (indexHash index)
      put (indexFile index)
      put (indexContent index)

   get = do
      hash <- get
      file <- get
      content <- get
      return $ Index hash file content


instance Serialize IndexArr where
   put (IndexArr arr) = do
      put arr

   get = do
      arr <- get
      return $ IndexArr arr


instance Show Index where
   show index = "(" ++ (indexHash index) ++
      ", " ++ (indexFile index) ++ ")"

data IndexArr = IndexArr { indexArr :: [Index] }
                           deriving Show

newIndexArr :: [Index] -> IndexArr
newIndexArr arr = IndexArr arr

newIndex hash file content = Index hash file content

putIndexArr :: IndexArr -> IO ()
putIndexArr iarr = do
   let arr = indexArr iarr
       bs2 = encode arr
   indexPath <- S.indexPath
   B.writeFile indexPath bs2

createIndex :: IO ()
createIndex = do
   indexArr <- getWorkingTree
   saveIndexes indexArr


saveIndexes :: IndexArr -> IO ()
saveIndexes arr = do
   let indexes = indexArr arr
       bs = encode $ newIndexArr (L.sort indexes)
   indexPath <- S.indexPath
   B.writeFile indexPath bs

getIndexed :: IO IndexArr
getIndexed = do
   indexPath <- S.indexPath
   exists <- doesFileExist indexPath
   case exists of
        False -> return (IndexArr [])
        True -> do
           bs <- B.readFile indexPath
           let Right arr = decode bs
           return arr

addAllFiles :: IO ()
addAllFiles = do
   indexed <- getIndexed
   wtree <- getWorkingTree
   let unindexed = getUnindexed indexed wtree
   mapM_ (addFile . indexFile) (indexArr unindexed)

addFile :: FilePath -> IO ()
addFile filePath' = do
   arr <- getIndexed
   rootDir <- S.rootDir
   let filePath = case (head filePath') == '/' of
                       True -> filePath'
                       False -> ('/' : filePath') 
   bs <- B.readFile $ rootDir ++ filePath
   let indexMap' = M.fromList $
            map (\ index -> (indexFile index, index))
                (indexArr arr)
       indexMap = M.delete filePath indexMap'
       hash = S.hashBs bs
   objectsDir <- S.objectsDir
   exists <- doesFileExist (objectsDir ++ "/" ++ hash)
   let mbs = S.if_ exists Nothing (Just bs)
       newi = newIndex hash filePath mbs
       indexMap2 = M.insert filePath newi indexMap
   saveIndexes $ newIndexArr (M.elems indexMap2)

rmFile :: FilePath -> IO ()
rmFile filePath = do
   arr <- getIndexed
   let indexMap = M.fromList $ map (\ i -> (indexFile i, i)) (indexArr arr)
       newMap = M.delete filePath indexMap
       newIndexes = M.elems newMap
   saveIndexes $ newIndexArr newIndexes


getWorkingTree :: IO IndexArr
getWorkingTree = do
   let gitDir = S.gitDir
   rdir <- S.rootDir
   pathArr <- recurDir rdir gitDir ""
   indexArr <- recurPathArr rdir pathArr
   return indexArr
   where
      recurPathArr rootDir [] = return $ newIndexArr []
      recurPathArr rootDir (path:pathArr) = do
         bs <- B.readFile $ rootDir ++ "/" ++ path
         objectsDir <- S.objectsDir
         let sum = S.hashBs bs
             objPath = objectsDir ++ "/" ++ sum
         exists <- doesFileExist objPath
         rest <- recurPathArr rootDir pathArr
         return $ newIndexArr
            (newIndex sum path (S.if_ exists Nothing (Just bs)) : indexArr rest)

      recurDir rootDir gdir path = do
         let realPath = rootDir ++ "/" ++ path
         case S.isGitDir path of
              True -> return []
              False -> do
                 isDir <- doesDirectoryExist realPath
                 if isDir == False
                    then return [path]
                    else do
                       content <- getDirectoryContents realPath
                       recurFiles path content
         where
            recurFiles dir [] = return []
            recurFiles dir (file:rest) = do
               arr <- case file of
                           ".." -> return []
                           "." -> return []
                           otherwise -> recurDir rootDir gdir $ dir ++ "/" ++ file
               result <- recurFiles dir rest
               return $ arr ++ result

getRemoved :: IndexArr -> IndexArr -> IndexArr
getRemoved arr1 arr2 =
   let map2 = M.fromList $ map
         (\ index -> (indexFile index, indexHash index))
         (indexArr arr2)
       removed = foldl
         (\ arr index ->
            let mexist = M.lookup (indexFile index) map2
            in maybe
               (index : arr)
               (\ _ -> arr)
               mexist)
         [] (indexArr arr1)
   in newIndexArr removed

getUnindexed :: IndexArr -> IndexArr -> IndexArr
getUnindexed indexed wtree =
   let indexMap = M.fromList $ map
         (\ index -> (indexFile index, indexHash index))
         (indexArr indexed)
       newIndexes = foldl (\ arr index ->
            let mexists = M.lookup (indexFile index) indexMap
            in maybe
               (index : arr)
               (\ hash -> if hash == (indexHash index)
                          then arr
                          else (index : arr)) mexists)
            [] (indexArr wtree)
   in newIndexArr newIndexes
