module HGit.Blob 
   ( Blob
   , blobContent
   , getContent
   , getBlob
   , getBlobs ) where

import HGit.Index
import qualified HGit.Shared as S
import Data.Typeable
import Data.Word
import Data.Serialize
import System.Directory
import qualified Data.Map as M
import qualified Data.ByteString as B

data Blob = Blob { blobContent :: B.ByteString } deriving Show

instance Serialize Blob where
   put (Blob content) = do
      put content

   get = do
      content <- get
      return $ Blob content

getContent :: Index -> IO B.ByteString
getContent index = 
   maybe 
      (do Just blob <- getBlob (indexHash index)
          return $ blobContent blob)
      (\ bs -> return bs)
      (indexContent index)

getBlob :: S.ObjectHash -> IO (Maybe Blob)
getBlob hash = do
   objectsDir <- S.objectsDir
   bs <- B.readFile (objectsDir ++ "/" ++ hash)
   let blob = decode bs
       result = either
         (\_ -> Nothing)
         (\b -> Just b)
         blob
   return result

getBlobs :: IO (M.Map S.ObjectHash Blob)
getBlobs = do
   indexes <- getIndexed
   let arr = indexArr indexes
       json = foldl (\ arr index -> 
                        let hash = indexHash index
                            mcontent = indexContent index
                        in maybe arr (\ content -> ((hash, Blob content):arr)) mcontent)
                     [] arr
   return $ M.fromList json


