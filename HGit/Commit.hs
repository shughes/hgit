module HGit.Commit
   ( Commit
   , commitParent
   , commitTree
   , commitAuthor
   , commitMessage

   , CommitParent
   , initCommitParent
   , initEmptyParent
   , commitParentHash
   , mergeParentHash
   , isEmptyParent

   , getLog
   , getLogByCommit
   , getLogByHash
   , getCommonCommit
   , getCommitTrail
   , getCommit
   , commit ) where

import HGit.Blob
import HGit.Tree
import HGit.Index
import HGit.Config
import qualified HGit.Shared as S
import Data.Serialize
import System.Directory
import Data.Time.Clock
import Data.Time.Format
import System.Locale
import qualified Data.List as L
import qualified Data.Map as M
import qualified Data.ByteString as B
import qualified Data.ByteString.UTF8 as B2


data Commit = Commit { commitParent :: CommitParent
                     , commitTree :: S.ObjectHash
                     , commitAuthor :: String
                     , commitMessage :: String
                     , commitTimestamp :: UTCTime }
                       deriving Eq

data CommitParent = CommitParent { commitParentHash :: S.ObjectHash }
                  | MergeParent { commitParentHash :: S.ObjectHash
                                , mergeParentHash :: S.ObjectHash }
                  | EmptyParent
                    deriving (Eq)

instance Show CommitParent where
   show (CommitParent c) = "CommitParent " ++ c
   show (MergeParent c m) = "MergeParent " ++ c ++ ", " ++ m
   show EmptyParent = "EmptyParent"

instance Show Commit where
   show commit = 
      "Author: " ++ (commitAuthor commit) ++ "\n" ++ 
      "Date: " ++ (show $ commitTimestamp commit) ++ "\n\n   " ++
      (commitMessage commit) ++ "\n" 
   

instance Serialize CommitParent where
   put (CommitParent c) = do
      putWord8 1
      put c

   put (MergeParent c m) = do
      putWord8 2
      put c
      put m

   put EmptyParent = putWord8 3

   get = do
      w <- getWord8
      case w of
           1 -> do c <- get
                   return (CommitParent c)
           2 -> do c <- get
                   m <- get
                   return (MergeParent c m)
           3 -> return EmptyParent

instance Serialize Commit where
   put (Commit p t a m time) = do
      put p
      put t
      put a
      put m
      let strTime = formatTime defaultTimeLocale "%c" time
      put strTime

   get = do
      p <- get
      t <- get
      a <- get
      m <- get
      strTime <- get
      let Just time = parseTime defaultTimeLocale "%c" strTime
      return $ Commit p t a m time

data Object = BlobType Blob
            | TreeType Tree

initCommitParent c = CommitParent c

isEmptyParent EmptyParent = True
isEmptyParent other = False

newMergeParent c m = MergeParent c m

initEmptyParent = EmptyParent

getCommonCommit :: CommitParent -> CommitParent -> IO (Maybe (S.ObjectHash, Commit))
getCommonCommit left right = do
   leftTrail <- recurParents left
   rightTrail <- recurParents right
   let sortedLeft = L.sortBy sortCommits $ L.nub leftTrail
       sortedRight = L.sortBy sortCommits $ L.nub rightTrail
       result = foldl
         (\ val commit ->
            case val of
                 Nothing -> S.if_ (L.elem commit sortedLeft)
                                  (Just commit)
                                  val
                 otherwise -> val)
         Nothing sortedRight
   return result

sortCommits (h1, commit1) (h2, commit2) =
   let time1 = commitTimestamp commit1
       time2 = commitTimestamp commit2
   in compare time2 time1

recurParents :: CommitParent -> IO [(S.ObjectHash, Commit)]
recurParents EmptyParent = return []
recurParents (MergeParent c m) = do
   cCommit <- getCommit c
   cResult <- recurParents (commitParent cCommit)
   mCommit <- getCommit m
   mResult <- recurParents (commitParent mCommit)
   return $ [(c, cCommit)] ++ cResult ++ [(m, mCommit)] ++ mResult
recurParents (CommitParent hash) = do
   commit <- getCommit hash
   result <- recurParents (commitParent commit)
   return $ [(hash, commit)] ++ result

getLog :: IO [(S.ObjectHash, Commit)]
getLog = do
   mhead <- S.getHeadHash
   let parent = maybe EmptyParent CommitParent mhead
   getLogByCommit parent

getLogByHash hash = do
   arr <- recurParents (initCommitParent hash)
   return $ L.sortBy sortCommits $ L.nub arr

getLogByCommit :: CommitParent -> IO [(S.ObjectHash, Commit)]
getLogByCommit hash = do
   arr <- recurParents hash
   return $ L.sortBy sortCommits $ L.nub arr

getCommitTrail :: CommitParent -> IO [Commit]
getCommitTrail EmptyParent = return []
getCommitTrail (MergeParent cHash mHash) = getCommitTrail (CommitParent cHash)
getCommitTrail (CommitParent hash) = do
   commit <- getCommit hash
   result <- getCommitTrail (commitParent commit)
   return $ [commit] ++ result


getCommit :: S.ObjectHash -> IO Commit
getCommit path = do
   objectsDir <- S.objectsDir
   bs <- B.readFile $ objectsDir ++ "/" ++ path
   let Right result = decode bs
   return result

getObjects :: IO (S.ObjectHash, M.Map S.ObjectHash Object)
getObjects = do
   (r, trees) <- getTrees
   blobs <- getBlobs
   let bList = map (\ (key, b) -> (key, BlobType b)) (M.toList blobs)
       tList = map (\ (key, t) -> (key, TreeType t)) (M.toList trees)
   return (r, M.fromList $ bList ++ tList)


commit :: String -> IO ()
commit message = do
   moldCommitHash <- S.getHeadHash
   (newRoot, all) <- getObjects
   (oldCommit', oldRoot) <- maybe
      (return (EmptyParent, ""))
      (\ oldHash -> do
         oldCommit <- getCommit oldHash
         return (initCommitParent oldHash, commitTree oldCommit))
      moldCommitHash

   -- determine if a post-merge commit
   maybeMerge <- S.getMergeBranch
   oldCommit <- maybe
      (return oldCommit')
      (\ mergeHash -> do
         S.rmMergeBranch
         return $ newMergeParent (commitParentHash oldCommit') mergeHash)
      maybeMerge

   case oldRoot == newRoot of
        True -> print "No changes have been made"
        False -> do
           mapM (writeObject all) (M.keys all)
           curTime <- getCurrentTime
           objectsDir <- S.objectsDir
           author <- getAuthor
           let c = Commit oldCommit newRoot author message curTime
               bs = encode c
               commitHash = S.hashBs bs
               newC = objectsDir ++ "/" ++ commitHash
           B.writeFile newC bs
           S.setHeadHash commitHash


   where
      handleCommon (BlobType blob) = encode blob
      handleCommon (TreeType tree) = encode tree

      writeObject all key = do
         objectsDir <- S.objectsDir
         let path = (objectsDir ++ "/" ++ key)
         exists <- doesFileExist path
         case exists of
              False -> do let Just common = M.lookup key all
                              bs = handleCommon common
                          case common of
                               TreeType t -> print key
                               otherwise -> return ()
                          B.writeFile path bs
              True -> return ()
