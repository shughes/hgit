module HGit.Stash where

import qualified HGit.Shared as S
import HGit.Stage
import HGit.Commit
import HGit.Index
import System.Directory

stashPush :: String -> IO ()
stashPush message = do
   indexed <- getIndexed
   wtree <- getWorkingTree
   let arr = getUnindexed indexed wtree
       stashArr = map indexFile (indexArr arr)
   origBranch <- S.getCurrentBranch
   Just origHash <- S.getHeadHash
   mapM_ addFile stashArr
   S.setCurrentBranch S.stashPath
   commit ("WIP: "++message)
   checkoutBranch origBranch 

stashPop :: IO ()
stashPop = do
   origBranch <- S.getCurrentBranch
   gitDir <- S.getGitDir
   exists <- doesFileExist (gitDir ++ "/" ++ S.stashPath)
   case exists of
        True -> do
            checkoutBranch S.stashPath
            log <- getLog
            let (_, fCommit) = head log
            case isEmptyParent (commitParent fCommit) of
                 True -> S.rmBranch S.stashPath
                 False -> S.setHeadHash (commitParentHash $ commitParent fCommit)
            S.setCurrentBranch origBranch
        False -> return ()

stashList :: IO [String]
stashList = do
   mhash <- S.getStashHash
   maybe (return []) (\ hash -> do
      commits <- getLogByCommit (initCommitParent hash)
      return $ map (commitMessage . snd) commits)
      mhash

